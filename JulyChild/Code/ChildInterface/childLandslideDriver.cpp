/**************************************************************************/
/**
**  childLandslideDriver.cpp: This provides a test and example of the CHILD
**  interface.
**
**  July 2008
**
**  For information regarding this program, please contact Greg Tucker at:
**
**     Cooperative Institute for Research in Environmental Sciences (CIRES)
**     and Department of Geological Sciences
**     University of Colorado
**     2200 Colorado Avenue, Campus Box 399
**     Boulder, CO 80309-0399
**
*/
/**************************************************************************/

#include "childInterface.h"

int main( int argc, char **argv )
{
  childInterface myChildInterface;
	
  myChildInterface.Initialize( argc, argv );
  tOption option( argc, argv );
  tInputFile inputFile( option.inputFile );

  // Example 2: using "RunOneStorm" 
  double mytime = 0;
  double myInitializationTime = inputFile.ReadDouble( "INITIALIZATION_TIME", 1000.0 );
//   double myInitializationTime = 1000.;
  // 		double myrunduration = 100000;
  // for initialization, turn off landslides and vegetation:
  myChildInterface.ChangeOption( "landslides",0 );
  myChildInterface.ChangeOption( "vegetation",0 );
  while( mytime<myInitializationTime )
    {
      mytime = myChildInterface.RunOneStorm();
    }
  // write out initialized soil depths with subsurface flow depths:
  myChildInterface.ChangeOption( "no fluvial",0 );
  myChildInterface.RunOneStorm();
  myChildInterface.WriteChildStyleOutput();
  // turn landslides back on:
  myChildInterface.ChangeOption( "landslides",1 );

  myChildInterface.RunOneStorm();
  //********************finally...
  //**11-13 by kun
  //**set a longer duration of simulation, or should we use just a signle storm??
 // myChildInterface.Run(0);
  //or, this is just for re-routing the stream network after all the initialization process?
  myChildInterface.WriteChildStyleOutput();
	
  // Note that calling CleanUp() isn't strictly necessary, as the destructor will automatically clean it
  // up when myChildInterface is deleted ... but it's nice to be able to do this at will (and free up
  // memory)
  myChildInterface.CleanUp();
	
  return 0;
}
