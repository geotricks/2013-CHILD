#newnew-------------------------------------------------------------------
#
# basicDlim1.in
#
# Example CHILD input file configured for a simple,
# detachment-limited simulation in which a rapidly
# uplifted block is worn down over time.
#
#-------------------------------------------------------------------
#
# Run control parameters
#
# The following parameters control the name and duration of the run along
# with a couple of other general settings.
# 
OUTFILENAME: name of the run
elk-20m
RUNTIME: Duration of run (years)
1
OPINTRVL: Output interval (years)
1
#INITIALIZATION_TIME:
#0
SEED: Random seed used to generate storm sequence & mesh, etc (as applicable)
1
#
# Mesh setup parameters
#
# These parameters control the initial configuration of the mesh. Here you
# specify whether a new or existing mesh is to be used; the geometry and
# resolution of a new mesh (if applicable); the boundary settings; etc.
#
#  Notes:
#
#    OPTREADINPUT - controls the source of the initial mesh setup:
#                    10 = create a new mesh in a rectangular domain
#                    1 = read in an existing triangulation (eg, earlier run)
#                    12 = create a new mesh by triangulating a given set
#                        of (x,y,z,b) points
#    INPUTDATAFILE - use this only if you want to read in an existing
#                    triangulation, either from an earlier run or from
#                    a dataset.
#    INPUTTIME - if reading in a mesh from an earlier run, this specifies
#                    the time slice number
#
OPTREADINPUT: 10=create new mesh; 1=read existing run/file; 12=read point file
12
INPUTDATAFILE: name of file to read input data from (only if reading mesh)
n/a
INPUTTIME: the time which you want data from (needed only if reading mesh)
0
OPTINITMESHDENS: no. of densifying iterations applied to initial mesh (0=none)
0
POINTFILENAME: name of file containing x,y,z,b data (b=boundary status)
elk-10m.pts
INPUTTIME: the time which you want data from (needed only if reading mesh)
n/a
OPTINITMESHDENS: no. of densifying iterations applied to initial mesh (0=none)
0
X_GRID_SIZE: "length" of grid, meters
0
Y_GRID_SIZE: "width" of grid, meters
0
OPT_PT_PLACE: type of point placement; 0=uniform, 1=perturbed unif., 2=random
1
GRID_SPACING: mean distance between grid nodes, meters
0
NUM_PTS: for random grid, number of points to place
n/a
TYP_BOUND: open boundary;0=corner,1=side,2= sides,3=4 sides,4=specify
0
NUMBER_OUTLETS: number of specified outlets
0
OUTLET_X_COORD1
409274.0
OUTLET_Y_COORD1
4813107.5
OUTLET_X_COORD2
409611.0
OUTLET_Y_COORD2
4813147.0
OPTSTREAMLINEBNDY: option to convert streamlines into open boundaries
0
NUM_STREAMLINES: number of streamlines for streamline boundaries
0
MEAN_ELEV: mean initial elevation
n/a
RAND_ELEV: max amplitude of random noise appied to initial topography
n/a
SLOPED_SURF: Option for sloping initial surface
n/a
UPPER_BOUND_Z: elevation along upper boundary
n/a
#
#   Climate parameters
#
OPTVAR: Option for rainfall variation
0
ST_PMEAN: Mean rainfall intensity (m/yr) (16.4 m/yr = Atlanta, GA)
380
ST_STDUR: Mean storm duration (yr)
10
ST_ISTDUR: Mean time between storms (yr)
0
ST_OPTSINVAR:
0
OPTSINVARINFILT: option for sinusoidal variations in infiltration capacity
0
#
#   Various options
#
OPTMEANDER: Option for meandering
0
OPTDETACHLIM: Option for detachment-limited erosion only
1
OPTREADLAYER: option to read layer information from file (only if reading mesh)
0
OPTLAYEROUTPUT:
0
OPT_NEW_LAYERSOUTPUT: option for writing layer bulk density (default "false")
0
OPT_NEW_LAYERSINPUT: option for reading layer bulk density (default "false")
0
OPTINTERPLAYER: for node moving, do we care about tracking the layers? yes=1
0
FLOWGEN: flow generation option: 0=Hortonian, 1=subsurface flow, etc.
0
LAKEFILL: fill lakes if = 1
1
TRANSMISSIVITY: for shallow subsurface flow option
0
INFILTRATION: infiltration capacity (for Hortonian option) (m/yr)
30000.0
OPTINLET: 1=add an "inlet" discharge boundary condition (0=none)
0
INDRAREA: inlet drainage area
25e6
INSEDLOAD:
1
INSEDLOAD1:
0.1
INSEDLOAD2:
0.9
INLET_X: x location of inlet
5000
INLET_Y: y location
9900
INLET_OPTCALCSEDFEED:
1
INLET_SLOPE:
0.02
OPTTSOUTPUT: option for writing mean erosion rates, etc, at each time step
0
TSOPINTRVL: not currently operational
100
OPTSTRATGRID: option for tracking stratigraphy in underlying regular grid
0
SURFER: optional output for Surfer graphics
0
#
#   Erosion and sediment transport parameters
#   (note: choice of sediment-transport law is dictated at compile-time;
#    see tErosion.h)
#
#   Important notes on parameters:
#
#   (1) kb, kt, mb, nb and pb are defined as follows:
#         E = kb * ( tau - taucrit ) ^ pb,
#         tau = kt * q ^ mb * S ^ nb,
#         q = Q / W,  W = Wb ( Q / Qb ) ^ ws,  Wb = kw Qb ^ wb
#      where W is width, Q total discharge, Qb bankfull discharge,
#      Wb bankfull width. Note that kb, mb and nb are NOT the same as the
#      "familiar" K, m, and n as sometimes used in the literature.
#
#   (2) For power-law sediment transport, parameters are defined as follows:
#         capacity (m3/yr) = kf * W * ( tau - taucrit ) ^ pf
#         tau = kt * q ^ mf * S ^ nf
#         q is as defined above
#
#   (3) KT and TAUC are given in SI units -- that is, time units of seconds
#       rather than years. The unit conversion to erosion rate or capacity
#       is made within the code.
#
OPTNOFLUVIAL: option to turn off fluvial processes
1
DETACHMENT_LAW:
4
TRANSPORT_LAW:
7
KF: sediment transport efficiency factor (dims vary but incl's conversion s->y)
617.0
MF: sediment transport capacity discharge exponent
0.66667
NF: sed transport capacity slope exponent (ND)
0.66667
PF: excess shear stress (sic) exponent
1.5
KB: bedrock erodibility coefficient (dimensions in m, kg, yr)
0.003
KR: regolith erodibility coefficient (dimensions same as KB)
100
KT:  Shear stress (or stream power) coefficient (in SI units)
987.3
MB: bedrock erodibility specific (not total!) discharge exponent
0.66667
NB: bedrock erodibility slope exponent
0.66667
PB: Exponent on excess erosion capacity (e.g., excess shear stress)
1
TAUCB: critical shear stress for detachment-limited-erosion (kg/m/s^2)
0
TAUCR: critical shear stress for detachment of regolith
0
TAUCD: critical shear stress for detachment-limited-erosion (kg/m/s^2)
30
KD: diffusivity coef (m2/yr)
1
OPTDIFFDEP: if =1 then diffusion only erodes, never deposits
0
OPTNODIFFUSION
1
OPT_NONLINEAR_DIFFUSION
1
CRITICAL_SLOPE: critical slope for nonlinear diffusion
1.25
OPT_DEPTH_DEPENDENT_DIFFUSION: Option for depth-dependent diffusion
1
DIFFDEPTHSCALE: depth scale for soil depth-dependent diffusion (m)
0.38
DIFFUSIONTHRESHOLD: used by "tools" models
0
DIFFUSIONTHRESHOLD: area above which no diffusion calculation (if non-zero) (m2)
0.0
BETA: fraction of sediment to bedload
0
#
#   Bedrock and regolith
#
BEDROCKDEPTH: initial depth of bedrock (make this arbitrarily large)
1000000.0
REGINIT: initial regolith thickness
0.05
MAXREGDEPTH: maximum depth of a single regolith layer (also "active layer")
100.0
#
#   Chemical and physical weathering
#
CHEM_WEATHERING_LAW: possible values 0-1: 0 = None; 1 = Dissolution
0
MAXDISSOLUTIONRATE: maximum dissolution rate (kg/m3/yr)
0.099
CHEMDEPTH: depth scale for dissolution (m)
0.18
ROCKDENSITYINIT: initial rock bulk density (kg/m3)
2270
PRODUCTION_LAW: possible values 0-2: 0 = None; 1 = exponential law; 2 = exp. with density dep.
0
SOILPRODRATE: zero-depth soil production rate (m/yr)
0.000268
SOILPRODRATEINTERCEPT: density-dependent soil production rate intercept (m/yr)
0.00055
SOILPRODRATESLOPE: density-dependent soil production rate slope ( (m/yr)/(kg/m3) )
0.00000017
SOILPRODDEPTH: depth scale for soil production rate (m)
0.30
SOILBULKDENSITY: bulk density of soil (constant) (kg/m3)
740
#
#   Tectonics / baselevel boundary conditions
#
OPTNOUPLIFT: option to turn off uplift (1 = no uplift)
1
UPTYPE: type of uplift (0=none, 1=uniform, 2=block, etc)
0
UPDUR: duration of uplift (yrs)
1e7
UPRATE: uplift rate (m/yr)
0
ACCEL_REL_UPTIME
0
FAULT_PIVOT_DISTANCE
15000
VERTICAL_THROW
1100.0
FAULTPOS: Fault position (m) (does not apply to all uplift functions)
30000
SUBSRATE:
0.0
DISLOCDEPTHUP
0.1
DISLOCDEPTHDOWN
15
DISLOCDIP
45
DISLOCSLIPRATE
0.05
ZMESHTHRESHOLD
100
#
#   Grain size parameters
#
#   (note: for Wilcock sand-gravel transport formula, NUMGRNSIZE must be 2.
#    If >1, a multiple-grain-size transport law must be used.)
#
NUMGRNSIZE: number of grain size classes
1
REGPROPORTION1: proportion of sediments of grain size diam1 in regolith [.]
1.0
BRPROPORTION1: proportion of sediments of grain size diam1 in bedrock [.]
1.0
GRAINDIAM1: representative diameter of first grain size class [m]
0.001
REGPROPORTION2: proportion of sediments of grain size diam2 in regolith [.]
0.1
BRPROPORTION2: proportion of sediments of grain size diam2 in bedrock [.]
0.5
GRAINDIAM2: representative diameter of second grain size class [m]
0.05
HIDINGEXP:
0.75
#
#   Hydraulic geometry parameters
#
#   Width is the most critical parameter as it is used in erosion and
#   transport capacity calculations. HYDR_WID_COEFF_DS is the "kw" parameter
#   referred to above (equal to bankfull width in m at unit bankfull discharge
#   in cms)
#
#   CHAN_GEOM_MODEL options are:
#     1 = empirical "regime" model: Wb = Kw Qb ^ wb, W / Wb = ( Q / Qb ) ^ ws
#     2 = Parker width closure: tau / tauc = const
#
CHAN_GEOM_MODEL: option for channel width closure
1
HYDR_WID_COEFF_DS: coeff. on downstream hydraulic width relation (m/(m3/s)^exp)
10.0
HYDR_WID_EXP_DS: exponent on downstream hydraulic width relation 
0.5
HYDR_WID_EXP_STN: exp. on at-a-station hydraulic width relation
0.5
HYDR_DEP_COEFF_DS: coeff. on downstream hydraulic depth relation (m/(m3/s)^exp)
1.0
HYDR_DEP_EXP_DS: exponent on downstream hydraulic depth relation 
0
HYDR_DEP_EXP_STN: exp. on at-a-station hydraulic depth relation
0
HYDR_ROUGH_COEFF_DS: coeff. on downstrm hydraulic roughness reln. (manning n)
0.03
HYDR_ROUGH_EXP_DS: exp. on downstream hydraulic roughness
0
HYDR_ROUGH_EXP_STN: exp on at-a-station hydr. rough.
0
BANK_ROUGH_COEFF: coeff. on downstream bank roughness relation (for meand only)
15.0
BANK_ROUGH_EXP: exp on discharge for downstream bank roughness (for meand only)
0.80
BANKFULLEVENT: precipitation rate of a bankfull event, in m/y
13.6
##################################################################
####  Vegetation (incl. forest, fire) options and parameters  ####
##################################################################
OPTVEG: option for dynamic vegetation growth and erosion
0
OPTGRASS_SIMPLE: option for old veg. cover model
0
#
# Forest options and parameters
#
OPTFOREST: option for dynamic forest growth
0
ROOTDECAY_K: decay const for root strength after death ( 1/year )
0.5
ROOTDECAY_N: decay time exponent for root strength after death ([])
0.73
ROOTGROWTH_A: growth const "a" ([])
0.95
ROOTGROWTH_B: growth const "b" ([])
19.05
ROOTGROWTH_C: growth const "c" ([])
-0.05
ROOTGROWTH_F: growth time const "f" ( 1/year )
0.25
ROOTSTRENGTH_J: inverse of root depth scale (1/meters)
2.6
MAXVERTROOTCOHESION: max. vertical root cohesion (Pa)
2100
MAXLATROOTCOHESION: max. lateral root cohesion (Pa)
4900
TREEHEIGHTINDEX: tree height at 100 yrs (fr. Duan) (meters)
40.0
VEGWEIGHT_MAX: max total biomass (Pa) (fr. Duan)
3000
VEGWEIGHT_A: biomass coeff "a" (fr. Duan)
0.952
VEGWEIGHT_B: biomass coeff "b" (fr. Duan)
19.05
VEGWEIGHT_C: biomass coeff "c" (fr. Duan)
-0.05
VEGWEIGHT_K: biomass coeff "k" (fr. Duan) (1/yrs)
0.12
WOODDENSITY: density of wood (kg/m3)
450.0
BLOWDOWNPARAM: 0.5 * RHO * coeff. of drag * (wind speed per rainrate)^2 / crown width per height
0.0
#1.0e+14
BLOW_SEED: Random seed for tree blowdown
875
TREEDIAM_B0: tree diameter coeff "b_0" (m)
74.0
TREEDIAM_B1: tree diameter coeff "b_1" (1/m)
-0.0105
TREEDIAM_B2: tree diameter coeff "b_2" ()
0.911
WOODDECAY_K: wood decay rate constant (1/yrs) (fr. Harmon et al., 1986, for Doug. Fir)
0.031
INITSTANDAGE: initial stand age (yrs)
0.0
#
# Fire options and parameters
#
OPTFIRE
0
OPTRANDOMFIRES: option for random fire intervals (1=random, 0=regular)
0
IFRDUR: Mean time between fires (yr) (or actual time if fires not random)
200
FSEED: Random seed for fires
8549307
##################################################################
####  Landsliding options and parameters                      ####
# For landsliding, need to set OPT_LANDSLIDES = 1 
# _AND_ ( FLOWGEN = 1 _AND_ OPTVAR_TRANSMISSIVITY = 1 )
# _OR_ FLOWGEN = 6
##################################################################
OPT_LANDSLIDES: option for landsliding; yes = 1, no = 0
0
OPT_3D_LANDSLIDES: option for 3D landsliding with earth pressures; yes = 1
0
DF_RUNOUT_RULE: debris flow runout; 0 = none, 1 = to outlet
0
DF_SCOUR_RULE: debris flow scour; 0 = none, 1 = all sediment
0
DF_DEPOSITION_RULE: debris flow deposition; 0 = none
0
FRICSLOPE: tangent of (internal deformation) friction angle ([])
0.9004
#atan(0.9004)=42 degrees (from Iverson, '97)
#
#   Other options
#
OPTFLOODPLAIN: option for overbank deposition using modified Howard 1992 model
0
OPTLOESSDEP: space-time uniform surface accumulation of sediment (loess)
0
OPTEXPOSURETIME: option for tracking surface-layer exposure ages
0
OPTKINWAVE: kinematic-wave flow routing (steady, 2D)
0
OPTMESHADAPTDZ: dynamic adaptive meshing based on erosion rates
0
OPTMESHADAPTAREA: dynamic adaptive meshing based on drainage area
0
OPTFOLDDENS: Option for mesh densification around a growing fold
0
OPT_TRACK_WATER_SED_TIMESERIES
0
OPT_FREEZE_ELEVATIONS: option to enable running model without changing elevations
1
Comments here:
#
