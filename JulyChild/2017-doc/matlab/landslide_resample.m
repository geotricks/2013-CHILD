activeNode = size(lsgep,1);

%count non-road nodes
nonroadcounter =0;
 for i=3:activeNode
     if lsgep(i)~=-9999
         nonroadcounter= nonroadcounter + 1;
     end
 end
 
   %count node with "1" landslideflg
 landslidecount =0;
 for i=1:activeNode
     if lsnode(i)==1
         landslidecount= landslidecount + 1;
     end
 end 
 
%create new matrice for data 
LSinfo = zeros (nonroadcounter,9);
sorted_LSinfo = zeros (nonroadcounter,9);
j=1;
for i= 3 :activeNode;
    if (lsgep(i)~= -9999)
        LSinfo(j,1) = lsgep(i);
        LSinfo(j,2) = lsnode(i);
        LSinfo(j,3) = varea(i);
        LSinfo(j,4) = forest(i);
        LSinfo(j,5) = slope(i);
        LSinfo(j,6) = al(i);
        LSinfo(j,7) = curvature(i);
        %3m
        LSinfo(j,8) = nodes(i-2,1)- 121159.3716;
        
        %1m
        LSinfo(j,9) = nodes(i-2,2) - 201382.4268;
        %3m
        %LSinfo(j,9) = nodes(i-2,2) - 201386.32824;
        %2m
        %LSinfo(j,9) = nodes(i-2,2) - 201382.329264;
        %4m
        %LSinfo(j,9) = nodes(i-2,2) - 201384.2556;
        %5m
        %LSinfo(j,9) = nodes(i-2,2) - 201381.32952;
        %6m
        %LSinfo(j,9) = nodes(i-2,2) - 201382.79256;
        %7m
        %LSinfo(j,9) = nodes(i-2,2) - 201384.328752;
        %8m
        %LSinfo(j,9) = nodes(i-2,2) - 201383.21928;        
        %9m
        %LSinfo(j,9) = nodes(i-2,2) - 201381.0552;           
        
        %10m
        %LSinfo(j,9) = nodes(i-2,2) - 201386.32824;       
        
        
        
                
        j=j+1;
    end
     
end

i=0;
j=0;
It=zeros(round(max(nodes(:,1)) - min(nodes(:,1))), round(max(nodes(:,2)) -min(nodes(:,2))) )-9999;
for k=1:nonroadcounter
j=round(LSinfo(k,8)/10)+1;
i=round(LSinfo(k,9)/10)+1;
    if LSinfo(k,1)>It(j,i)
        It(j,i)=LSinfo(k,1);
    end
end

Itv = It(It>-9999);
Itv = sort(Itv);
%sort them
%sorted_LSinfo = sortrows(LSinfo,[1 2]);


uniqueItv = unique(Itv);
totalnode=length(uniqueItv);
FA=zeros(totalnode,1) ;
for n=1:totalnode
    FA(n)=n/totalnode;
end

itnodes=zeros(136,1);
n=0;
for j=1:nonroadcounter
    if LSinfo(j,2)==1
    n=n+1;
    itnodes(n,1)= LSinfo(j,1);
    
    end
end

itnodes=sort(itnodes);

%flipdim to make figure looks better
itnodes=flipdim(itnodes,1);
uniqueItv=flipdim(uniqueItv,1);

FA3=interp1(uniqueItv,FA,itnodes, 'spline' );

Fls=zeros(136,1);
for j=1:136
    Fls(j)=j/136;
end

Flsr=[0;Fls;1];
FA1mr=[0;FA3;1];

plot(FA1mr,Flsr,'DisplayName','Flsr vs. FA3r','XDataSource','FA3r','YDataSource','Flsr');figure(gcf)

% 
% sorted_LSinfo = sortrows(LSinfo,[1 2]);
% sssorted_LSinfo = flipdim(sorted_LSinfo,1);
% 
% totalarea=0;
% for i=1:nonroadcounter
%     totalarea=totalarea+sssorted_LSinfo(i,3);
%     sssorted_LSinfo(i,10)=totalarea;
% end
% 
% for i=1:nonroadcounter
%     sssorted_LSinfo(i,10)=sssorted_LSinfo(i,10)/totalarea;
% end
% 
% nn=0;
% Fla10m_ori=zeros(136,1);
% for j=1:nonroadcounter
%     if sssorted_LSinfo(j,2)==1
%     nn=nn+1;
%     Fla10m_ori(nn,1)= sssorted_LSinfo(j,10);
%     
%     end
% end
%Fla10m_ori=[0;Fla10m_ori;1];