a=32.8778104960705;
b=63.1488264352446;
c=14.2969561276106;
d=268.799376599888;

syms x;
raw = d+(a-d)/(1+(x/c)^b) ;
wt=diff(raw)

%wt
%=(36886082384984256119565351455337*((17592186044416*aa)/251514712065779)^(4373334868363769/70368744177664))/(35397548860551246832061120512*(((17592186044416*aa)/251514712065779)^(4443703612541433/70368744177664)+ 1)^2);
  sorted_LSinfo(nonroadcounter,12)=0;

%matlabpool open 
%parfor i=1:nonroadcounter

p_mean = 3.28511E-05;
for i=1:nonroadcounter
    aa= sorted_LSinfo(i,1);
    %id for quick reference
    sorted_LSinfo(i,10)=i;
      
    if aa>0
    %sorted_LSinfo(i,8)=subs(wt,aa);
    lswt=(36886082384984256119565351455337*((17592186044416*aa)/251514712065779)^(4373334868363769/70368744177664))/(35397548860551246832061120512*(((17592186044416*aa)/251514712065779)^(4443703612541433/70368744177664)+ 1)^2);
    sorted_LSinfo(i,11)=lswt;
    %Pls
    sorted_LSinfo(i,12)=lswt*sorted_LSinfo(i,3)*p_mean;
    else
    sorted_LSinfo(i,11)=-0.9999;
    end

end
plot(sorted_LSinfo(:,1),sorted_LSinfo(:,11));


mc_init=zeros(nonroadcounter,4);
for i=1:nonroadcounter
    %id
mc_init(i,1)=sorted_LSinfo(i,10);
    %it
mc_init(i,2)=sorted_LSinfo(i,1);
    %va
mc_init(i,3)=sorted_LSinfo(i,3);
    %Pls
mc_init(i,4)=sorted_LSinfo(i,12);
end