a=13.4145506735331;
b=12.0711868499346;
c=18.7328914430739;
d=176.105677135538;

syms x;
raw = d+(a-d)/(1+(x/c)^b) ;
wt=diff(raw)

%wt
%=(19449275822021638679573090353037*((281474976710656*aa)/5272840182662473)^(6232524121489325/562949953421312))/(185521570951741339578228801536*(((281474976710656*aa)/5272840182662473)^(6795474074910637/562949953421312) + 1)^2);
 
sorted_LSinfo(nonroadcounter,12)=0;

%matlabpool open 
%parfor i=1:nonroadcounter

p_mean = 3.18664E-05;
for i=1:nonroadcounter
    aa= sorted_LSinfo(i,1);
    %id for quick reference
    sorted_LSinfo(i,10)=i;
      
    if aa>0
    %sorted_LSinfo(i,8)=subs(wt,aa);
    lswt=(19449275822021638679573090353037*((281474976710656*aa)/5272840182662473)^(6232524121489325/562949953421312))/(185521570951741339578228801536*(((281474976710656*aa)/5272840182662473)^(6795474074910637/562949953421312) + 1)^2);
    sorted_LSinfo(i,11)=lswt;
    %Pls
    sorted_LSinfo(i,12)=lswt*sorted_LSinfo(i,3)*p_mean;
    else
    sorted_LSinfo(i,11)=-0.9999;
    end

end
plot(sorted_LSinfo(:,1),sorted_LSinfo(:,11));


mc_init=zeros(nonroadcounter,4);
for i=1:nonroadcounter
    %id
mc_init(i,1)=sorted_LSinfo(i,10);
    %it
mc_init(i,2)=sorted_LSinfo(i,1);
    %va
mc_init(i,3)=sorted_LSinfo(i,3);
    %Pls
mc_init(i,4)=sorted_LSinfo(i,12);
end