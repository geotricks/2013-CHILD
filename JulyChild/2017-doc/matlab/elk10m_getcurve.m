a=-1.50184394217561;
b=1.06299819792816;
c=235.595463486132;
d=142.177784149104;



syms x;
raw = d+(a-d)/(1+(x/c)^b) ;
wt=diff(raw)

%wt
%=(12100611204806475458757398605685*((35184372088832*aa)/8289278449736901)^(141859330357137/2251799813685248))/(18665795668702695050124824936448*(((35184372088832*aa)/8289278449736901)^(2393659144042385/2251799813685248) + 1)^2);
 
sorted_LSinfo(nonroadcounter,13)=0;

p_mean=1.53952E-05;

for i=1:nonroadcounter
    aa= sorted_LSinfo(i,1);
    %id for quick reference
    sorted_LSinfo(i,10)=i;
    if aa>2
    %sorted_LSinfo(i,11)=subs(wt,aa);
    

    lswt=d+(a-d)/(1+(aa/c)^b) ;
    sorted_LSinfo(i,11)=lswt;
    sorted_LSinfo(i,12)=lswt*sorted_LSinfo(i,3)*p_mean;
        if sorted_LSinfo(i,4)==9
            sorted_LSinfo(i,13)=lswt*sorted_LSinfo(i,3)*p_mean*1.5;
        else
            sorted_LSinfo(i,13)=lswt*sorted_LSinfo(i,3)*p_mean*0.5;
        end
    else
        lswt=0.5524*exp(0.1889*aa);
        sorted_LSinfo(i,11)=lswt;
        sorted_LSinfo(i,12)=lswt*sorted_LSinfo(i,3)*p_mean;
        if sorted_LSinfo(i,4)==9
            sorted_LSinfo(i,13)=lswt*sorted_LSinfo(i,3)*p_mean*1.5;
        else
            sorted_LSinfo(i,13)=lswt*sorted_LSinfo(i,3)*p_mean*0.5;
        end
    end

end
plot(sorted_LSinfo(:,1),sorted_LSinfo(:,11));
 

mc_init=zeros(nonroadcounter,4);
for i=1:nonroadcounter
    %id
mc_init(i,1)=sorted_LSinfo(i,10);
    %it
mc_init(i,2)=sorted_LSinfo(i,1);
    %va
mc_init(i,3)=sorted_LSinfo(i,3);
    %Pls
mc_init(i,4)=sorted_LSinfo(i,12);
%Pls with forest age
mc_init(i,5)=sorted_LSinfo(i,13);
end