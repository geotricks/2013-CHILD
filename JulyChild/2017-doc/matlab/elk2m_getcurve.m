a=10.2712748503483;
b=5.35185376348205;
c=12.5791459044348;
d=584.96529412471;

syms x;
raw = d+(a-d)/(1+(x/c)^b) ;
wt=diff(raw)

%wt=(1791767236380940361222106863113*((281474976710656*aa)/3540714800490729)^(4899751746897163/1125899906842624))/(7328107470641198167316496384*(((281474976710656*aa)/3540714800490729)^(6025651653739787/1125899906842624)
%+ 1)^2);
 
sorted_LSinfo(nonroadcounter,13)=0;

p_mean=1.53952E-05;

for i=1:nonroadcounter
    aa= sorted_LSinfo(i,1);
    %id for quick reference
    sorted_LSinfo(i,10)=i;
    if aa>0
    %sorted_LSinfo(i,11)=subs(wt,aa);
    
    lswt=(1791767236380940361222106863113*((281474976710656*aa)/3540714800490729)^(4899751746897163/1125899906842624))/(7328107470641198167316496384*(((281474976710656*aa)/3540714800490729)^(6025651653739787/1125899906842624)+ 1)^2);
    sorted_LSinfo(i,11)=lswt;
    sorted_LSinfo(i,12)=lswt*sorted_LSinfo(i,3)*p_mean;
        if sorted_LSinfo(i,4)==9
            sorted_LSinfo(i,13)=lswt*sorted_LSinfo(i,3)*p_mean*1.5;
        else
            sorted_LSinfo(i,13)=lswt*sorted_LSinfo(i,3)*p_mean*0.5;
        end
    else
    sorted_LSinfo(i,11)=-0.9999;
    end

end
plot(sorted_LSinfo(:,1),sorted_LSinfo(:,11));
 

mc_init=zeros(nonroadcounter,5);
for i=1:nonroadcounter
    %id
mc_init(i,1)=sorted_LSinfo(i,10);
    %it
mc_init(i,2)=sorted_LSinfo(i,1);
    %va
mc_init(i,3)=sorted_LSinfo(i,3);
    %Pls
mc_init(i,4)=sorted_LSinfo(i,12);
%Pls with forest age
mc_init(i,5)=sorted_LSinfo(i,13);
end