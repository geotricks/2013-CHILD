mc_num = 100;
%create a new matrix and copy gep and varea into it
mc_result = zeros(nonroadcounter, 4+mc_num*2);
for i=1:nonroadcounter
%id
mc_result(i,1)=mc_init(i,1);
%it
mc_result(i,2)=mc_init(i,2);
%va
mc_result(i,3)=mc_init(i,3);
end


k=3;
for j = 1:mc_num
    %synthetic=rand(nonroadcounter,1);
    mclscounter =0;
    k=k+2;
    for i=1:nonroadcounter
        
        R=rand();
        Pls = mc_init(i,5);
        if (Pls > R)
            mc_result(i,k)=1;
            mclscounter=mclscounter+1;
            mc_result(i,k+1)=mclscounter;
        else
            mc_result(i,k)=-9999;
            mc_result(i,k+1)=mclscounter;
        end
    end
    mclscounter;
    
    for i=1:nonroadcounter
    mc_result(i,k+1)=mc_result(i,k+1)/mclscounter;
    end

end

%getFls

itnodes=zeros(136,1);
n=0;
for j=1:nonroadcounter
    if LSinfo(j,2)==1
    n=n+1;
    itnodes(n,1)= LSinfo(j,1);
    
    end
end
uniqueItv = unique(sort(itnodes));

Flsi=zeros(length(uniqueItv),mc_num);
n=4;
[rr,ii,jj]=unique(mc_result(:,2));
for i=1:mc_num
    n=n+1;
    Flsi(:,i)=interp1(rr,mc_result(ii,i+n),uniqueItv);
end

Flscf=zeros(136,3);
for i=1:136;
    
    Flscf(i,1)=prctile(Flsi(i,:),2.5);
    Flscf(i,2)=prctile(Flsi(i,:),97.5);
    Flscf(i,3) = i/136;
end

cellno=0;
for i=1:359301
	if sorted_LSinfo(i,1)>0
	cellno=cellno+1;
	end
	

end




figure
hold all;
plot(uniqueItv,Flscf(:,1));
plot(uniqueItv,Flscf(:,3));
plot(uniqueItv,Flscf(:,2));
% plot(wtfit,wtfls);
legend('2.5','ls','97.5');
hold off;
