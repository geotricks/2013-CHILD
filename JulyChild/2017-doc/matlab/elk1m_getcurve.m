a=20.7577545964798;
b=7.70028293318936;
c=7.16093133622723;
d=1563.13684596435;

syms x;
raw = d+(a-d)/(1+(x/c)^b) ;
wt=diff(raw)

%wt =(4900903679581387521864464410401*((1125899906842624*x)/8062491924364665)^(1885961982574281/281474976710656))/(2954934539896349162922311680*(((1125899906842624*x)/8062491924364665)^(2167436959284937/281474976710656) + 1)^2)
sorted_LSinfo(nonroadcounter,12)=0;

%matlabpool open 
%parfor i=1:nonroadcounter

p_mean = 4.31499E-05;
for i=1:nonroadcounter
    aa= sorted_LSinfo(i,1);
    %id for quick reference
    sorted_LSinfo(i,10)=i;
      
    if aa>0
    %sorted_LSinfo(i,8)=subs(wt,aa);
    lswt=(4900903679581387521864464410401*((1125899906842624*aa)/8062491924364665)^(1885961982574281/281474976710656))/(2954934539896349162922311680*(((1125899906842624*aa)/8062491924364665)^(2167436959284937/281474976710656) + 1)^2);
    sorted_LSinfo(i,11)=lswt;
    %Pls
    sorted_LSinfo(i,12)=lswt*sorted_LSinfo(i,3)*p_mean;
    else
    sorted_LSinfo(i,11)=-0.9999;
    end

end
plot(sorted_LSinfo(:,1),sorted_LSinfo(:,11));


mc_init=zeros(nonroadcounter,4);
for i=1:nonroadcounter
    %id
mc_init(i,1)=sorted_LSinfo(i,10);
    %it
mc_init(i,2)=sorted_LSinfo(i,1);
    %va
mc_init(i,3)=sorted_LSinfo(i,3);
    %Pls
mc_init(i,4)=sorted_LSinfo(i,12);
end