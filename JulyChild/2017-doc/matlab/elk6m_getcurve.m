A=-0.000567861801810697;
B=2.41682957658506;
C=32.824732579436;
D=131.939358682476;



 
sorted_LSinfo(nonroadcounter,13)=0;

p_mean=2.80729E-05;
%p_mean=1.22464E-05;


for i=1:nonroadcounter
    aa= sorted_LSinfo(i,1);
    %id for quick reference
    sorted_LSinfo(i,10)=i;
    if aa>0
    %sorted_LSinfo(i,11)=subs(wt,aa);

    lswt=((A-D)/(1+((aa/C)^B))) + D ;
    sorted_LSinfo(i,11)=lswt;
    sorted_LSinfo(i,12)=lswt*sorted_LSinfo(i,3)*p_mean;
        if sorted_LSinfo(i,4)==9
            sorted_LSinfo(i,13)=lswt*sorted_LSinfo(i,3)*p_mean*1.5;
        else
            sorted_LSinfo(i,13)=lswt*sorted_LSinfo(i,3)*p_mean*0.5;
        end
    else
    sorted_LSinfo(i,11)=-0.9999;
    end

end
plot(sorted_LSinfo(:,1),sorted_LSinfo(:,11));
 

mc_init=zeros(nonroadcounter,4);
for i=1:nonroadcounter
    %id
mc_init(i,1)=sorted_LSinfo(i,10);
    %it
mc_init(i,2)=sorted_LSinfo(i,1);
    %va
mc_init(i,3)=sorted_LSinfo(i,3);
    %Pls
mc_init(i,4)=sorted_LSinfo(i,12);
%Pls with forest age
mc_init(i,5)=sorted_LSinfo(i,13);
end